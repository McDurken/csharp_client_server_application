﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Windows.Forms;

namespace SimpleClient
{
    public partial class ClientForm1 : Form
    {
        public ClientForm1()
        {
            InitializeComponent();
        }

        private void btnSendRequest_Click(object sender, EventArgs e)
        {
            // you could choose to run StartClient in a
            // task to avoid locking the ui
            StartClient();


        } // End btnSendRequest_Click()

        private void StartClient()
        {

            Socket client = null;
            try
            {
                // 1. client needs ip address and port number
                // of the server.
                IPAddress ipaddress = IPAddress.Parse(txtServerIP.Text);
                int port = int.Parse(txtPort.Text);

                // 2. Create an endPoint to the server
                EndPoint remoteEndPoint = new IPEndPoint(ipaddress, port);

                // 3. create the same type of socket as the server
                client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // 4. bind the socket to the endpoint
                client.Connect(remoteEndPoint);

                // 5. send request to server
                // use the Send method in the Socket class
                string request = txtRequest.Text;
                byte[] data = Encoding.UTF8.GetBytes(request);
                client.Send(data);

                // 6. recieve response from server
                // create a byte array will be populated by 
                // data coming from server
                byte[] buffer = new byte[1024];
                int bytesRecieved = client.Receive(buffer);
                // teh recieve method returns the acual number of
                // bytes sent by the server

                // convert bytes to string
                string response = Encoding.UTF8.GetString(buffer, 0, bytesRecieved);

                // display
                rtbResponse.Text = response;

            }
            catch (FormatException fe)
            {
                MessageBox.Show(fe.Message);
            }
            catch (SocketException se)
            {

                MessageBox.Show(se.Message);
            }
            finally
            {
                // shutdown and close the socket
                if (client != null)
                {
                    client.Shutdown(SocketShutdown.Both);
                    client.Close();
                }
                
            }

        } // End StartClient()
    }
}
