﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace SimpleServer
{
    public partial class ServerForm1 : Form
    {
        public ServerForm1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Establish the local endpoint for the server
            ///socket.
            string hostname = Dns.GetHostName();
            IPHostEntry hostentry = Dns.GetHostEntry(hostname);
            IPAddress[] ipaddresses = hostentry.AddressList;

            IPAddress ipaddress = GetIPV4(ipaddresses);
            int port = 9999;
            //1. create the server endPoint
            EndPoint endPoint = new IPEndPoint(ipaddress, port);
            //2. create the socket
            Socket listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            //3. bind the endpoint to the socket
            listener.Bind(endPoint);
            //4. set the server in listening mode
            //the listen method starts a background thread that
            //monitors or listens for incoming requests from clients
            //these requests are queued into an internal queue that
            //can hold 20 requests at a time.
            listener.Listen(20);
            listBox1.Items.Add(String.Format(
                "Server is listening at {0}:{1}", ipaddress, port));

            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    // use the Accept method to retrieve the
                    // next client from the queue for 
                    // processing

                    Socket aclient = listener.Accept();

                    // if no client is in the queue, the
                    // Accept method blocks. Waits until a
                    // client is queued in.
                    DisplayClientInfo(aclient);
                    // process the client
                    Task.Factory.StartNew(() => ProcessClient(aclient));


                } // End Infinite Loop

            });
            
        }


        // SetText for listbox task
        private void SetText(string text)
        {

            if (listBox1.InvokeRequired)
            {
                Action<string> action = SetText;
                this.Invoke(action, text);
            }
            else
            {
                listBox1.Items.Add(text);
                listBox1.SelectedIndex = listBox1.Items.Count - 1;
            }

        } // End SetText()

        // this method gets information about the client
        // such as client ipaddress
        private void DisplayClientInfo(Socket client)
        {

            // get the client endpint using the
            // RemoteEndPoint property
            IPEndPoint clientEndP = (IPEndPoint)client.RemoteEndPoint;

            // client ip address
            IPAddress clientIPAddress = clientEndP.Address;

            // client port
            int clientPort = clientEndP.Port;

            // get client domain name
            string clientName = Dns.GetHostEntry(clientIPAddress).HostName;

            SetText(String.Format("Request from client: {0} at {1}: {2}", clientName, clientIPAddress, clientPort));
        } // End DisplayCLientInfo()

        // method to process client request
        private void ProcessClient(Socket aclient)
        {
            // Save aclient
            Socket client = aclient;
            
            try
            {
                // 1. get/recieve client request
                byte[] buffer = new byte[1024];
                int bytesrecieved = client.Receive(buffer);
                string request = Encoding.UTF8.GetString(buffer, 0, bytesrecieved);

                // 2. process the request
                // expecting request in the form of a command followed by
                // 2 numeric values
                string[] tokens = request.Split(new char[] { ' ', ','}, StringSplitOptions.RemoveEmptyEntries);

                double result = 0;
                string response = String.Empty;

                switch (tokens[0].ToLower())
                {
                    case "add":
                        {
                            double v1, v2;
                            double.TryParse(tokens[1], out v1);
                            double.TryParse(tokens[2], out v2);
                            result = v1 + v2;
                            response = result.ToString();
                        }
                        break;
                    case "sub":
                        {
                            double v1, v2;
                            double.TryParse(tokens[1], out v1);
                            double.TryParse(tokens[2], out v2);
                            result = v1 - v2;
                            response = result.ToString();
                        }
                        break;
                    case "mul":
                        {
                            double v1, v2;
                            double.TryParse(tokens[1], out v1);
                            double.TryParse(tokens[2], out v2);
                            result = v1 * v2;
                            response = result.ToString();
                        }
                        break;
                    case "div":
                        {
                            double v1, v2;
                            double.TryParse(tokens[1], out v1);
                            double.TryParse(tokens[2], out v2);
                            result = v1 / v2;
                            response = result.ToString();
                        }
                        break;
                    case "pow":
                        {
                            double v1, v2;
                            double.TryParse(tokens[1], out v1);
                            double.TryParse(tokens[2], out v2);
                            result = Math.Pow(v1, v2);
                            response = result.ToString();
                        }
                        break;
                    default:
                        response = "Invalid Command";
                        break;
                }

                // 3. send a response (echo the response back)
                byte[] data = Encoding.UTF8.GetBytes(response);
                client.Send(data);


            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }
            finally
            {
                // shutdown and close the socket
                if (client != null)
                {
                    client.Shutdown(SocketShutdown.Both);
                    client.Close();
                }
            }


        } // End ProcessClient()

        //helper method that takes the ipaddresses array
        //and return an ipv4 format
        private IPAddress GetIPV4(IPAddress[] ipaddresses)
        {
            foreach(IPAddress address in ipaddresses)
            {
                if (address.GetAddressBytes().Length == 4)
                    return address;
            }
            return null;
        }
    }
}
///read about Socket class
///and about its send and receive methods
